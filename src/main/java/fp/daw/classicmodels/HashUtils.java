package fp.daw.classicmodels;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;
import java.util.Base64;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

public class HashUtils {

	private static SecureRandom secureRandom = new SecureRandom();

	public static byte[] getRandomSalt(int length) {
		byte[] salt = new byte[length];
		secureRandom.nextBytes(salt);
		return salt;
	}

	public static byte[] getHash(String password, int lenght, byte[] salt)
			throws InvalidKeySpecException, NoSuchAlgorithmException {
		KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 0xffff, lenght);
		SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		SecretKey key = factory.generateSecret(spec);
		return key.getEncoded();
	}

	public static String getBase64Hash(String password) throws InvalidKeySpecException, NoSuchAlgorithmException {
		byte[] salt = getRandomSalt(66);
		byte[] hash = getHash(password, 512, salt);
		String hashBase64 = Base64.getEncoder().encodeToString(hash);
		String saltBase64 = Base64.getEncoder().encodeToString(salt);
		return hashBase64.substring(0, 21) + saltBase64 + hashBase64.substring(21);
	}

	public static boolean validateBase64Hash(String password, String savedHash)
			throws InvalidKeySpecException, NoSuchAlgorithmException {
		byte[] salt = Base64.getDecoder().decode(savedHash.substring(21, 109));
		byte[] hash1 = Base64.getDecoder().decode(savedHash.substring(0, 21) + savedHash.substring(109));
		byte[] hash2 = getHash(password, 512, salt);
		return Arrays.compare(hash1, hash2) == 0;
	}

	// PARTE OPCIONAL CON LOS M�TODOS PARA CODIFICAR Y VALIDAR EN HEXADECIMAL

	public static String getHexadecimalHash(String password) throws InvalidKeySpecException, NoSuchAlgorithmException {
		byte[] salt = getRandomSalt(20);
		byte[] hash = getHash(password, 256, salt);
		String saltHexa = encodeHexString(salt);
		String hashHexa = encodeHexString(hash);
		return hashHexa.substring(0, 21) + saltHexa + hashHexa.substring(21);
	}

	public static boolean validatehexa(String password, String savedHash)
			throws InvalidKeySpecException, NoSuchAlgorithmException {
		String saltHexa = savedHash.substring(21, 61);
		byte[] salt = decodeHexString(saltHexa);
		String hash1 = savedHash.substring(0, 21) + savedHash.substring(61);
		byte[] hash = getHash(password, 256, salt);
		String hash2 = encodeHexString(hash);
		return hash1.equals(hash2);
	}

	public static String encodeHexString(byte[] byteArray) {
		StringBuffer hexStringBuffer = new StringBuffer();
		for (int i = 0; i < byteArray.length; i++) {
			hexStringBuffer.append(byteToHex(byteArray[i]));
		}
		return hexStringBuffer.toString();
	}

	public static byte hexToByte(String hexString) {
		int firstDigit = toDigit(hexString.charAt(0));
		int secondDigit = toDigit(hexString.charAt(1));
		return (byte) ((firstDigit << 4) + secondDigit);
	}

	private static int toDigit(char hexChar) {
		int digit = Character.digit(hexChar, 16);
		if (digit == -1) {
			throw new IllegalArgumentException("Invalid Hexadecimal Character: " + hexChar);
		}
		return digit;
	}

	public static byte[] decodeHexString(String hexString) {
		if (hexString.length() % 2 == 1) {
			throw new IllegalArgumentException("Invalid hexadecimal String supplied.");
		}

		byte[] bytes = new byte[hexString.length() / 2];
		for (int i = 0; i < hexString.length(); i += 2) {
			bytes[i / 2] = hexToByte(hexString.substring(i, i + 2));
		}
		return bytes;
	}

	public static String byteToHex(byte num) {
		char[] hexDigits = new char[2];
		hexDigits[0] = Character.forDigit((num >> 4) & 0xF, 16);
		hexDigits[1] = Character.forDigit((num & 0xF), 16);
		return new String(hexDigits);
	}

	public static String getBase64Digest(String message) throws NoSuchAlgorithmException {
		byte[] hash = MessageDigest.getInstance("SHA-256").digest(message.getBytes());
		return Base64.getUrlEncoder().encodeToString(Arrays.copyOf(hash, 33));
		/* retorna una cadena de longitud 44 */
	}

}
