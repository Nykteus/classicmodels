package fp.daw.classicmodels;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.mail.MessagingException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

@WebServlet("/reenvio")
public class ResendServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		String email = request.getParameter("email");
		if (session.getAttribute("usuario") != null)
			response.sendRedirect("catalogo.jsp");
		else {
			Connection con = null;
			PreparedStatement stm = null;
			ResultSet rs = null;

			try {
				Context context = new InitialContext();
				DataSource ds = (DataSource) context.lookup("java:comp/env/jdbc/classicmodels");
				con = ds.getConnection();
				stm = con.prepareStatement("select confirmationCode from signups where customerEmail = ?");
				stm.setString(1, email);
				rs = stm.executeQuery();
				rs.next();
				String confirmationCode = rs.getString(1);
				resendConfirmationMessage(email, confirmationCode);
				response.sendRedirect("catalogo.jsp");
			} catch (SQLException | NamingException | MessagingException e) {
			} finally {
				if (rs != null)
					try {
						rs.close();
					} catch (SQLException e) {
					}
				if (stm != null)
					try {
						stm.close();
					} catch (SQLException e) {
					}
				if (con != null)
					try {
						con.close();
					} catch (SQLException e) {
					}
			}
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

	public static void resendConfirmationMessage(String email, String confirmationCode) throws MessagingException {
		StringBuilder body = new StringBuilder();
		body.append("<p>Pulsa el el botón para validar tu cuenta y poder acceder a nuestros servicios</p>");
		body.append("<form action=\"http://localhost:8080/ClassicModels/confirm\"");
		body.append(" method=\"post\">");
		body.append("<input type=\"hidden\" name=\"email\" value=\"");
		body.append(email);
		body.append("\" />");
		body.append("<input type=\"hidden\" name=\"cc\" value=\"");
		body.append(confirmationCode);
		body.append("\" />");
		body.append("<p><input type=\"submit\" value=\"Confirmar\" /></p>");
		body.append("</form>");
		
		MailService.sendMessage("smtp.gmail.com", // servidor SMPT
				"587", // puerto
				"classicmodelsfp@gmail.com", // cuenta para iniciar sesión en el servidor SMTP de Gmail
				"FP2021cifp", // contraseña para iniciar sesión en el servidor SMTP de Gmail
				email, // dirección del destinatario
				"Confirma tu dirección de correo electrónico", // asunto
				body.toString(), // cuerpo del mensaje
				"text/html" // tipo de contenido
		);
	}

}
