package fp.daw.classicmodels;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

@WebServlet("/postbuy")
public class PostBuy extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		Connection con = null;
		PreparedStatement stm = null;
		ResultSet rs = null;

		try {
			Context context = new InitialContext();
			DataSource ds = (DataSource) context.lookup("java:comp/env/jdbc/classicmodels");
			con = ds.getConnection();
			Map<String, LineaDetalle> cart = (Map<String, LineaDetalle>) session.getAttribute("cart");
			int customer = (int) session.getAttribute("customerNumber");
			stm = con.prepareStatement("select orderNumber from orders order by orderNumber desc limit 1");
			rs = stm.executeQuery();
			int order = 0;
			if (rs.next()) {
				order = rs.getInt(1);
			}
			order++;
			if (stm != null)
				try {
					stm.close();
				} catch (SQLException e) {
			}
			stm = con.prepareStatement(
					"insert into orders(orderNumber, orderDate, requiredDate, shippedDate, status, comments, customerNumber) values (?, ?, ?, ?, ?, ?, ?)");
			stm.setInt(1, order);
			stm.setDate(2, new Date(System.currentTimeMillis()));
			stm.setDate(3, new Date(System.currentTimeMillis()));
			stm.setNull(4, java.sql.Types.DATE);
			stm.setString(5, "Procesando");
			stm.setString(6, "El pedido se enviará en breves");
			stm.setInt(7, customer);
			stm.execute();
			if (stm != null)
				try {
					stm.close();
				} catch (SQLException e) {
				}
			int contador = 1;
			for (Map.Entry<String, LineaDetalle> articulo : cart.entrySet()) {
				LineaDetalle arti = articulo.getValue();
				stm = con.prepareStatement(
						"insert into orderdetails(orderNumber, productCode, quantityOrdered, priceEach, orderLineNumber) values (?, ?, ?, ?, ?)");
				stm.setInt(1, order);
				stm.setString(2, arti.getProductCode());
				stm.setInt(3, arti.getAmount());
				float price = (float) (arti.getAmount() * arti.getPrice());
				stm.setFloat(4, price);
				stm.setInt(5, contador);
				stm.execute();
				contador++;
			}
			response.sendRedirect("postcompra.jsp");
		} catch (SQLException | NamingException e) {
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
				}
			if (stm != null)
				try {
					stm.close();
				} catch (SQLException e) {
				}
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
				}
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
