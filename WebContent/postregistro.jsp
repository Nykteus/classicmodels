<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<c:choose>
	<c:when test="${not empty sessionScope.usuario}">
		<c:redirect url="catalogo.jsp" />
	</c:when>
	<c:otherwise>
		<%
		String firstName = request.getParameter("nombre");
		String email = request.getParameter("email");
		if (firstName == null || email == null)
			response.sendRedirect("catalogo.jsp");
		else {
		%>

		<!DOCTYPE html>
		<html>
<head>
<meta charset="ISO-8859-1">
<title>Post-Registro</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>
	<section class="container">
		<div class="container-fluid">
			<div class="jumbotron">
				<div class="display-4 text-center">
					<p>
						<img src="miniatura.jpg" width="20%" height="20%"
							class="rounded-circle mx-auto d-block" alt="miniatura-coche" />
					</p>
					Post-Registro
				</div>
			</div>
		</div>
		<div class="container-fluid">
			<nav class="navbar navbar-expand-lg bg-dark navbar-dark">
				<a class="navbar-brand" href="login.jsp">Inicio</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse"
					data-target="#navbarSupportedContent"
					aria-controls="navbarSupportedContent" aria-expanded="false"
					aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav mr-auto">
						<li class="nav-item"><a class="nav-link"
							href="catalogo.jsp?inicio=0&fin=19">Maquetas</a></li>
					</ul>
					<c:choose>
						<c:when test="${not empty sessionScope.usuario}">
							<p class="navbar-brand">
								Bienvenid@ ${sessionScope.usuario} <a class="text-white"
									href="logout.jsp">Cerrar Sesión</a>
							</p>
						</c:when>
						<c:otherwise>
							<a class="navbar-brand" href="login.jsp">Iniciar Sesión</a>
							<a class="navbar-brand" href="registro.jsp">Regístrate</a>
						</c:otherwise>
					</c:choose>
				</div>

			</nav>
		</div>
		<div class="container-fluid">
			<h1>
				Bienvenid@
				<%
			if (firstName != null) {
				out.println(firstName);
			} else {
				if (session.getAttribute("nombre") != null) {
					out.println(session.getAttribute("nombre"));
				} else {
					response.sendRedirect("catalogo.jsp");
				}
			}
			%>
			</h1>
			<p>
				Sólo queda un paso más para completar tu registro y tener acceso a
				todos nuestros servicios, sigue las instrucciones que te hemos
				enviado a <strong> <%
 if (email != null) {
 	out.println(email);
 } else {
 	if (session.getAttribute("email") != null) {
 		out.println(session.getAttribute("email"));
 	} else {
 		response.sendRedirect("catalogo.jsp");
 	}
 }
 %>
				</strong>
			<p>Si no has recibido el mensaje pulsa el botón para Reenviar el
				código de confirmación</p>
			<form action="reenvio" method="post">
				<div class="form-group text-center">
					<input type="hidden" name="email" value="<%out.print(email);%>" />
					<input type="submit" class="btn btn-primary" value="Reenviar" />
				</div>
			</form>
		</div>
	</section>
</body>
		</html>
		<%
		}
		%>
	</c:otherwise>
</c:choose>