<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UT F-8"
	pageEncoding="UTF-8"%>
<sql:query var="modelos" dataSource="jdbc/classicmodels">
    select productCode, productName, productScale, productDescription, MSRP from products;
</sql:query>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Catálogo de Productos</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>
	<section class="container">
		<div class="container-fluid">
			<div class="jumbotron">
				<div class="display-4 text-center">
					<p>
						<img src="miniatura.jpg" width="20%" height="20%"
							class="rounded-circle mx-auto d-block" alt="miniatura-coche" />
					</p>
					Catálogo de Modelos a Escala
				</div>
			</div>
		</div>
		<div class="container-fluid">
			<nav class="navbar navbar-expand-lg bg-dark navbar-dark">
				<a class="navbar-brand" href="login.jsp">Inicio</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse"
					data-target="#navbarSupportedContent"
					aria-controls="navbarSupportedContent" aria-expanded="false"
					aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav mr-auto">
						<li class="nav-item"><a class="nav-link"
							href="catalogo.jsp?inicio=0&fin=19">Maquetas</a></li>


						<c:choose>
							<c:when test="${not empty sessionScope.usuario}">
								<li class="nav-item"><a class="nav-link" href="cart.jsp">Carrito</a></li>
						</ul>
						<p class="navbar-brand">
							Bienvenid@ ${sessionScope.usuario} <a class="text-white"
								href="logout.jsp">Cerrar Sesión</a>
						</p>
						</c:when>
						<c:otherwise>
							</ul>
							<a class="navbar-brand" href="login.jsp">Iniciar Sesión</a>
							<a class="navbar-brand" href="registro.jsp">Regístrate</a>
						</c:otherwise>
					</c:choose>
				</div>

			</nav>
		</div>
		<br />
		<%
		/*<%! Esta forma añade una especie de variable global/sesión, también usado para funciones, seguir investigando
				<% Sirve para declarar funciones, variables...
				<%= Sirve para asignar valores de las variables dentro de los módulos de jsp>*/
		int contador = 0;
		%>
		<c:forEach var="modelo" items="${modelos.rows}">
			<%
			contador++;
			%>
		</c:forEach>
		<%
		int paginas = 0;
		paginas = contador / 20;
		if (contador % 20 != 0) {
			paginas++;
			contador = paginas * 20;
		}
		int inicio = 0;
		int fin = 19;
		if (request.getParameter("inicio") != null && request.getParameter("fin") != null) {
			inicio = Integer.parseInt(request.getParameter("inicio"));
			fin = Integer.parseInt(request.getParameter("fin"));
			boolean valido = false;
			for (int x = 0; x < contador ; x += 20) {
				if (inicio == x) {
					valido = true;
				}
			}
			if (valido == true) {
				valido = false;
				for (int y = 19; y < contador ; y += 20) {
					if (fin == y) {
						valido = true;
					}
				}
				int diferencia = fin - inicio;
				if(fin == contador){
					valido = true;
				}
				if (inicio > fin || diferencia > 19) {
					valido = false;
				}
				if (valido == false) {
					inicio = 0;
					fin = 19;
					response.sendRedirect("http://localhost:8080/ClassicModels/catalogo.jsp?inicio=" + inicio + "&fin=" + fin);
				}
			} else {
				inicio = 0;
				fin = 19;
				response.sendRedirect("http://localhost:8080/ClassicModels/catalogo.jsp?inicio=" + inicio + "&fin=" + fin);
			}
		}
		%>
		<!-- 
			Se puede omitir la etiqueta c:out y dejar solo la variable
			para añadir imagenes mejor svg, se crea carpeta en web content con nombre img
			para el carrito de la compra creamos etiqueta a  con la imagen y redireccion al servlet cart pasando el codigo de producto, el precio y el nombre
			Solo si se ha iniciado sesion por lo tanto sar etiquetas c:if previas
		 -->
		<c:forEach var="modelo" items="${modelos.rows}" begin="<%= inicio %>"
			end="<%= fin %>">
			<div class="row justify-content-center">
				<div class="col-8 bg-success">
					<h3>
						<c:out value="${modelo.productName}" />
					</h3>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-2 bg-primary">
					<div class="row bg-danger">
						<p class="col align-top">
							Escala
							<c:out value="${modelo.productScale}" />
						</p>
					</div>
					<div class="row bg-primary">
						<p class="col align-items-end">
							Precio
							<c:out value="${modelo.MSRP}" />
						</p>
					</div>
				</div>
				<div class="col-6 bg-secondary">
					<p>
						<c:out value="${modelo.productDescription}" />
					</p>
				</div>
			</div>
			<c:choose>
				<c:when test="${not empty sessionScope.usuario}">
					<div class="row justify-content-center">
						<div class="col-8">
							<a type="button" class="btn btn-info btn-block"
								href="cart?c=${modelo.productCode}&n=${modelo.productName}&p=${modelo.MSRP}&l=0">
								<img style="width: 1em; height: 1em;" src="img/cart-plus.svg" />
							</a>

						</div>
					</div>
				</c:when>
			</c:choose>
			<br />
		</c:forEach>
		<ul class="pagination justify-content-center">
			<%
			int paginaInicio = 0;
			int paginaFinal = 19;
			for (int x = 1; x <= paginas; x++) {
			%>
			<li class="page-item"><a class="page-link"
				href="catalogo.jsp?inicio=<%=paginaInicio%>&fin=<%=paginaFinal%>"><%=x%></a></li>
			<%
			paginaInicio += 20;
			paginaFinal += 20;
			}
			%>
		</ul>
	</section>
</body>
</html>