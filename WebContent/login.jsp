<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" session="true"%>
<c:if test="${not empty sessionScope.usuario }">
	<c:redirect url="catalogo.jsp" />
</c:if>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Inicio de Sesi�n</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>
	<section class="container">
		<div class="container-fluid">
			<div class="jumbotron">
				<div class="display-4 text-center">
					<p>
						<img src="miniatura.jpg" width="20%" height="20%"
							class="rounded-circle mx-auto d-block" alt="miniatura-coche" />
					</p>
					Inicio de Sesi�n
				</div>
			</div>
		</div>
		<div class="container-fluid">
			<nav class="navbar navbar-expand-lg bg-dark navbar-dark">
				<a class="navbar-brand" href="login.jsp">Inicio</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse"
					data-target="#navbarSupportedContent"
					aria-controls="navbarSupportedContent" aria-expanded="false"
					aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav mr-auto">
						<li class="nav-item"><a class="nav-link"
							href="catalogo.jsp?inicio=0&fin=19">Maquetas</a></li>
					</ul>
					<a class="navbar-brand" href="registro.jsp">Reg�strate</a>
				</div>

			</nav>
		</div>
		<br />
		<form action="auth" method="post">
			<div class="form-group text-center">
				<div class="row justify-content-center text-left">
					<div class="col-4">
						<c:choose>
							<c:when test="${param.status == 1}">
								<span class="text-danger">El usuario y/o la contrase�a no
									son correctos</span>
								<br>
							</c:when>
							<c:when test="${param.status == 2}">
								<span class="text-danger">Fallo del Servidor</span>
								<br>
							</c:when>
						</c:choose>
						<label for="email">Email</label> <input type="email" name="email"
							class="form-control" placeholder="Introduce tu Email"
							width="200px" required="required" /><br> <label
							for="password">Contrase�a</label> <input type="password"
							name="password" class="form-control"
							placeholder="Introduce tu Contrase�a" width="200px"
							required="required" /><br>
					</div>
				</div>
				<input type="submit" class="btn btn-primary" value="Login" />
			</div>
		</form>
	</section>
</body>
</html>