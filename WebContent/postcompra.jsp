<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<c:if test="${empty sessionScope.usuario}">
	<c:redirect url="catalogo.jsp" />
</c:if>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Factura</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>
	<section class="container">
		<div class="container-fluid">
			<div class="jumbotron">
				<div class="display-4 text-center">
					<p>
						<img src="miniatura.jpg" width="20%" height="20%"
							class="rounded-circle mx-auto d-block" alt="miniatura-coche" />
					</p>
					Factura
				</div>
			</div>
		</div>
		<div class="container-fluid mb-2">
			<nav class="navbar navbar-expand-lg bg-dark navbar-dark">
				<a class="navbar-brand" href="login.jsp">Inicio</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse"
					data-target="#navbarSupportedContent"
					aria-controls="navbarSupportedContent" aria-expanded="false"
					aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav mr-auto">
						<li class="nav-item"><a class="nav-link"
							href="catalogo.jsp?inicio=0&fin=19">Maquetas</a></li>
						<li class="nav-item"><a class="nav-link" href="cart.jsp">Carrito</a></li>
					</ul>
					<p class="navbar-brand">
						Bienvenid@ ${sessionScope.usuario} <a class="text-white"
							href="logout.jsp">Cerrar Sesión</a>
					</p>
				</div>
			</nav>
		</div>
		<div class="container-fluid w-75">
		<h1 class="display-3">Factura</h1>
			<table class="table">
				<thead>
					<tr>
						<th>Producto</th>
						<th>Unidades</th>
						<th>Precio</th>
						<th>Importe</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="linea" items="${sessionScope.cart}">
						<c:choose>
							<c:when test="${linea.value.amount > 0}">
								<tr>
									<td>${linea.value.productName}</td>
									<td>${linea.value.amount}</td>
									<td>${linea.value.price}</td>
									<td>${linea.value.amount * linea.value.price}</td>
								</tr>
							</c:when>
						</c:choose>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</section>
</body>
<% session.removeAttribute("cart"); %>
</html>