<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<c:if test="${not empty sessionScope.usuario }">
	<c:redirect url="catalogo.jsp" />
</c:if>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Registro de Usuarios</title>
<script>
	function check() {
		var pass = document.getElementById("password").value;
		var pass2 = document.getElementById("confirmar").value;
		if(pass == pass2){
			document.getElementById("error").style.display = "none";
			return true;
		}
		else{
			document.getElementById("error").style.display = "block";
			return false;
		}
	}
</script>
<title>Registro</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>
	<section class="container">
		<div class="container-fluid">
			<div class="jumbotron">
				<div class="display-4 text-center">
					<p>
						<img src="miniatura.jpg" width="20%" height="20%"
							class="rounded-circle mx-auto d-block" alt="miniatura-coche" />
					</p>
					Registro
				</div>
			</div>
		</div>
		<div class="container-fluid">
			<nav class="navbar navbar-expand-lg bg-dark navbar-dark">
				<a class="navbar-brand" href="login.jsp">Inicio</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse"
					data-target="#navbarSupportedContent"
					aria-controls="navbarSupportedContent" aria-expanded="false"
					aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav mr-auto">
						<li class="nav-item"><a class="nav-link"
							href="catalogo.jsp?inicio=0&fin=19">Maquetas</a></li>
					</ul>
					<a class="navbar-brand" href="login.jsp">Iniciar Sesisón</a>
				</div>

			</nav>
		</div>
		<br />
		<form action="signup" method=post onsubmit="return check()">
			<div class="form-group text-center">
				<div class="row justify-content-center text-left">
					<div class="col-4">
					<p class="text-danger" id="error" style="display: none;">Las contraseñas no coinciden</p>
						<c:choose>
							<c:when test="${param.status == 1}">
								<span class="text-danger">La dirección de correo ya está
									registrada</span>
									<script>
									document.getElementById("error").style.display = "none";
									</script>
								<br>
							</c:when>
							<c:when test="${param.status == 2}">
								<span class="text-danger">Fallo del Servidor, contacte
									con el administrador</span>
									<script>
									document.getElementById("error").style.display = "none";
									</script>
								<br>
							</c:when>
						</c:choose>
						<label for="name">Nombre</label> <input type="text" name="nombre"
							class="form-control" placeholder="Introduce tu Nombre"
							width="200px" required="required" /><br> <label
							for="apellidos">Apellidos</label> <input type="text"
							name="apellidos" class="form-control"
							placeholder="Introduce tus Apellidos" width="200px"
							required="required" /><br> <label for="email">Email</label>
						<input type="email" name="email" class="form-control"
							placeholder="Introduce tu Email" width="200px"
							required="required" /><br>
							<label for="password">Contraseña</label>
						<input type="password" name="password" id="password" class="form-control" placeholder="Introduce tu Contraseña" width="200px" required="required" /><br>
							<label for="password">Confirmar
							contraseña</label>
							<input type="password" name="password" class="form-control" id="confirmar" placeholder="Repite tu Contraseña" width="200px" required="required"/><br>
					</div>
				</div>
				<input type="submit" class="btn btn-primary" value="Registrar"/>
			</div>
		</form>
	</section>
</body>
</html>